#!/usr/bin/env bash

# Use este script para executar testes locais

RESULTS_WORKSPACE="$(pwd)/load-test/user-files/results"
GATLING_BIN_DIR="$HOME/gatling/3.10.4/bin"
GATLING_WORKSPACE="$(pwd)/load-test/user-files"

# Função para monitorar o uso de recursos
monitorResources() {
    docker stats --format "table {{.Name}}\t{{.MemPerc}}\t{{.CPUPerc}}" \
        --no-stream gatling docker java | \
        awk 'NR==1 || $2 > 70 || $2 > 80 || $2 > 90 || $2 > 100'
}

# Função para identificar o tempo de operação em diferentes faixas de utilização de CPU
identifyCPUUsage() {
    docker stats --format "table {{.Name}}\t{{.CPUPerc}}" \
        --no-stream gatling docker java | \
        awk 'NR>1 && $2 < 50 {low50++} $2 >= 50 && $2 < 60 {low60_70++} \
            $2 >= 60 && $2 < 70 {low70_80++} $2 >= 70 && $2 < 80 {low80_90++} \
            $2 >= 80 && $2 < 90 {low90_100++} $2 >= 90 {high90_100++} \
            $2 >= 100 && $2 < 150 {high100_150++} $2 >= 150 {high150_200++} \
            END {print "CPU Usage Distribution:"; \
                print "0-50%: ", low50, "seconds"; \
                print "50-60%: ", low60_70, "seconds"; \
                print "60-70%: ", low70_80, "seconds"; \
                print "70-80%: ", low80_90, "seconds"; \
                print "80-90%: ", low90_100, "seconds"; \
                print "90-100%: ", high90_100, "seconds"; \
                print "100-150%: ", high100_150, "seconds"; \
                print "150-200%: ", high150_200, "seconds"}'
}

# Função para executar os testes do Gatling
runGatling() { 
    sh "$GATLING_BIN_DIR/gatling.sh" -rm local -s RinhaBackendCrebitosSimulation \
        -rd "Rinha de Backend - 2024/Q1: Crébito" \
        -rf "$RESULTS_WORKSPACE" \
        -sf "$GATLING_WORKSPACE/simulations"
}

# Função para iniciar os testes
startTest() {
    for i in {1..20}; do
        # 2 requests para acordar as 2 instâncias da API :)
        curl --fail http://localhost:9999/clientes/1/extrato && \
        echo "" && \
        curl --fail http://localhost:9999/clientes/1/extrato && \
        echo "" && \
        runGatling && \
        break || sleep 2;
    done
}

# Iniciar monitoramento de recursos em segundo plano
monitorResources &

# Iniciar testes
startTest

# Identificar a utilização de CPU após os testes
identifyCPUUsage
