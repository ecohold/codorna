#codorna app        
        
        codorna/
        ├── docker-compose.yml
        ├── Dockerfile
        ├── docs
        │   └── API.md
        │   └── arquitetura.sh
        │   └── api.sh
        ├── LICENSE
        ├── load-test
        │   └── user-files
        │       └── executar-teste-local.sh
        │       └── simulations
        │           └── rinhabackend
        │               └── RinhaBackendCrebitosSimulation.scala
        ├── app.sh
        ├── processa.sh
        ├── main.c
        ├── libcodorna.so
        ├── nginx.conf
        └── README.md


0) app codorna; api rest crud, in-memory,concorrente, blocante, importa libcodorna.so e se torna mais performaticas que ocê
1) gere código padrão senior develop bash gcp cloud 
2) comentarios somente no código, servir como documentação, auditoria,date, versione, assine 
3) em português  bloco de codigo único , menos de 100 linhas, completo, testado, validado, produção/distribuição/funcional
4) com base na arquitetura e arvore de arquivos do 
5) aplique 23 design pattern e 10xtreme
6) arquivo: nome do arquivo: atenda os requisitos e lógica de negocio e testes. sim
6.1) sim ( )nota de 1 a 3 ( )
6.2) não ( ) porque, onde, linha(s) ( )
6.3) refatore, corrija, supere versão anterior
6.4) teste as mudanças, valide, audite, date, versione, assine, documente em /docs
7) crie o arquivo: nome do arquivo
8) exclua o arquivo: nome do arquivo
9) refatore o arquivo: nome do arquivo 
10)     main.c: api rest crud performatica in-memory completa base para libcodorna.so
10.1)   main.c apoi api rest crud performatica acoplada a db postress persistencia descricao
10.1.1) base para main.c: api rest crud performatica in-memory completa base para libcodorna.so
11) nginx.conf  híbrido (http e proxy reverso) 
12) app.sh: revecebe carga de nginx proxy e roteia para processa.sh
13) processa.sh: recebe carga repassa processamento para libcodorna.somente
14) Dockerfile
15) docker-compose.yml
16) libcodorna.so: cria estrutura de dados, popula, le, altera com base nas requisições de processa.sh devolve para 
17) executar-teste-local.sh cliente que gera requisições http na porta 9999 ao nginx load balance
    #runGatlin: 
    1) adicione um docker stats & ps para os processos gatling, java e docker
    2) toda vez que a memória ultrapassar 70%, 80%, 90% e 100%
    3) identificar todos processos e tempo que operaram abaixo de 50%; entre 50 e 60%, entre 60 e 70%,
        entre 70 e 80%, entre 80 e 90%, entre 90 e 100%, entre 100 e 150% entre 150 e 200% (o que acontece?)

runGatling() { 
    sh $GATLING_BIN_DIR/gatling.sh -rm local -s RinhaBackendCrebitosSimulation \
        -rd "Rinha de Backend - 2024/Q1: Crébito" \
        -rf $RESULTS_WORKSPACE \
        -sf "$GATLING_WORKSPACE/simulations"
}

startTest() {
    for i in {1..20}; do
        # 2 requests to wake the 2 api instances up :)
        curl --fail http://localhost:9999/clientes/1/extrato && \
        echo "" && \
        curl --fail http://localhost:9999/clientes/1/extrato && \
        echo "" && \
        runGatling && \
        break || sleep 2;
    done
}

startTest 


18) #---RinhaBackendCrebitosSimulation.scala---cliente app.sh gera trabalho/request/carga---#

#----------requisitos de teste-------------------#

import scala.concurrent.duration._

import scala.util.Random

import util.Try

import io.gatling.commons.validation._
import io.gatling.core.session.Session
import io.gatling.core.Predef._
import io.gatling.http.Predef._


class RinhaBackendCrebitosSimulation
  extends Simulation {

  def randomClienteId() = Random.between(1, 5 + 1)
  def randomValorTransacao() = Random.between(1, 10000 + 1)
  def randomDescricao() = Random.alphanumeric.take(10).mkString
  def randomTipoTransacao() = Seq("c", "d", "d")(Random.between(0, 2 + 1)) // not used
  def toInt(s: String): Option[Int] = {
    try {
      Some(s.toInt)
    } catch {
      case e: Exception => None
    }
  }

  val validarConsistenciaSaldoLimite = (valor: Option[String], session: Session) => {
    /*
      Essa função é frágil porque depende que haja uma entrada
      chamada 'limite' com valor conversível para int na session
      e também que seja encadeada com com jmesPath("saldo") para
      que 'valor' seja o primeiro argumento da função validadora
      de 'validate(.., ..)'.
      
      ========#trate bem disto em app.sh #==============
      
      Nota para quem não tem experiência em testes de performance:
        O teste de lógica de saldo/limite extrapola o que é comumente 
        feito em testes de performance apenas por causa da natureza
        da Rinha de Backend. Evite fazer esse tipo de coisa em 
        testes de performance, pois não é uma prática recomendada
        normalmente.
    */ 

    val saldo = valor.flatMap(s => Try(s.toInt).toOption)
    val limite = toInt(session("limite").as[String])

    (saldo, limite) match {
      case (Some(s), Some(l)) if s.toInt < l.toInt * -1 => Failure("Limite ultrapassado!")
      case (Some(s), Some(l)) if s.toInt >= l.toInt * -1 => Success(Option("ok"))
      case _ => Failure("WTF?!")
    }
  }

  val httpProtocol = http
    .baseUrl("http://localhost:9999")
    .userAgentHeader("Agente do Caos - 2024/Q1")

  val debitos = scenario("débitos")
    .exec {s =>
      val descricao = randomDescricao()
      val cliente_id = randomClienteId()
      val valor = randomValorTransacao()
      val payload = s"""{"valor": ${valor}, "tipo": "d", "descricao": "${descricao}"}"""
      val session = s.setAll(Map("descricao" -> descricao, "cliente_id" -> cliente_id, "payload" -> payload))
      session
    }
    .exec(
      http("débitos")
      .post(s => s"/clientes/${s("cliente_id").as[String]}/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s => s("payload").as[String]))
          .check(
            status.in(200, 422),
            status.saveAs("httpStatus"))
          .checkIf(s => s("httpStatus").as[String] == "200") { jmesPath("limite").saveAs("limite") }
          .checkIf(s => s("httpStatus").as[String] == "200") {
            jmesPath("saldo").validate("ConsistenciaSaldoLimite - Transação", validarConsistenciaSaldoLimite)
          }
    )

  val creditos = scenario("créditos")
    .exec {s =>
      val descricao = randomDescricao()
      val cliente_id = randomClienteId()
      val valor = randomValorTransacao()
      val payload = s"""{"valor": ${valor}, "tipo": "c", "descricao": "${descricao}"}"""
      val session = s.setAll(Map("descricao" -> descricao, "cliente_id" -> cliente_id, "payload" -> payload))
      session
    }
    .exec(
      http("créditos")
      .post(s => s"/clientes/${s("cliente_id").as[String]}/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s => s("payload").as[String]))
          .check(
            status.in(200),
            jmesPath("limite").saveAs("limite"),
            jmesPath("saldo").validate("ConsistenciaSaldoLimite - Transação", validarConsistenciaSaldoLimite)
          )
    )

  val extratos = scenario("extratos")
    .exec(
      http("extratos")
      .get(s => s"/clientes/${randomClienteId()}/extrato")
      .check(
        jmesPath("saldo.limite").saveAs("limite"),
        jmesPath("saldo.total").validate("ConsistenciaSaldoLimite - Extrato", validarConsistenciaSaldoLimite)
    )
  )

  val validacaConcorrentesNumRequests = 25
  val validacaoTransacoesConcorrentes = (tipo: String) =>
    scenario(s"validação concorrência transações - ${tipo}")
    .exec(
      http("validações")
      .post(s"/clientes/1/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s"""{"valor": 1, "tipo": "${tipo}", "descricao": "validacao"}"""))
          .check(status.is(200))
    )
  
  val validacaoTransacoesConcorrentesSaldo = (saldoEsperado: Int) =>
    scenario(s"validação concorrência saldo - ${saldoEsperado}")
    .exec(
      http("validações")
      .get(s"/clientes/1/extrato")
      .check(
        jmesPath("saldo.total").ofType[Int].is(saldoEsperado)
      )
    )

  val saldosIniciaisClientes = Array(
    Map("id" -> 1, "limite" ->   1000 * 100),
    Map("id" -> 2, "limite" ->    800 * 100),
    Map("id" -> 3, "limite" ->  10000 * 100),
    Map("id" -> 4, "limite" -> 100000 * 100),
    Map("id" -> 5, "limite" ->   5000 * 100),
  )

  val criterioClienteNaoEcontrado = scenario("validação HTTP 404")
    .exec(
      http("validações")
      .get("/clientes/6/extrato")
      .check(status.is(404))
    )

  val criteriosClientes = scenario("validações")
    .feed(saldosIniciaisClientes)
    .exec(
      /*
        Os valores de http(...) essão duplicados propositalmente
        para que sejam agrupados no relatório e ocupem menos espaço.
        O lado negativo é que, em caso de falha, pode não ser possível
        saber sua causa exata.
      */ 
      http("validações")
      .get("/clientes/#{id}/extrato")
      .check(
        status.is(200),
        jmesPath("saldo.limite").ofType[String].is("#{limite}"),
        jmesPath("saldo.total").ofType[String].is("0")
      )
    )
    .exec(
      http("validações")
      .post("/clientes/#{id}/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": "toma"}"""))
          .check(
            status.in(200),
            jmesPath("limite").saveAs("limite"),
            jmesPath("saldo").validate("ConsistenciaSaldoLimite - Transação", validarConsistenciaSaldoLimite)
          )
    )
    .exec(
      http("validações")
      .post("/clientes/#{id}/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s"""{"valor": 1, "tipo": "d", "descricao": "devolve"}"""))
          .check(
            status.in(200),
            jmesPath("limite").saveAs("limite"),
            jmesPath("saldo").validate("ConsistenciaSaldoLimite - Transação", validarConsistenciaSaldoLimite)
          )
    )
    .exec(
      http("validações")
      .get("/clientes/#{id}/extrato")
      .check(
        jmesPath("ultimas_transacoes[0].descricao").ofType[String].is("devolve"),
        jmesPath("ultimas_transacoes[0].tipo").ofType[String].is("d"),
        jmesPath("ultimas_transacoes[0].valor").ofType[Int].is("1"),
        jmesPath("ultimas_transacoes[1].descricao").ofType[String].is("toma"),
        jmesPath("ultimas_transacoes[1].tipo").ofType[String].is("c"),
        jmesPath("ultimas_transacoes[1].valor").ofType[Int].is("1")
      )
    )
    .exec( // Consistencia do extrato
      http("validações")
      .post("/clientes/#{id}/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": "danada"}"""))
          .check(
            status.in(200),
            jmesPath("saldo").saveAs("saldo"),
            jmesPath("limite").saveAs("limite")
          )
          .resources(
            // 5 consultas simultâneas ao extrato para verificar consistência
            http("validações").get("/clientes/#{id}/extrato").check(
              jmesPath("ultimas_transacoes[0].descricao").ofType[String].is("danada"),
              jmesPath("ultimas_transacoes[0].tipo").ofType[String].is("c"),
              jmesPath("ultimas_transacoes[0].valor").ofType[Int].is("1"),
              jmesPath("saldo.limite").ofType[String].is("#{limite}"),
              jmesPath("saldo.total").ofType[String].is("#{saldo}")
            ),
            http("validações").get("/clientes/#{id}/extrato").check(
              jmesPath("ultimas_transacoes[0].descricao").ofType[String].is("danada"),
              jmesPath("ultimas_transacoes[0].tipo").ofType[String].is("c"),
              jmesPath("ultimas_transacoes[0].valor").ofType[Int].is("1"),
              jmesPath("saldo.limite").ofType[String].is("#{limite}"),
              jmesPath("saldo.total").ofType[String].is("#{saldo}")
            ),
            http("validações").get("/clientes/#{id}/extrato").check(
              jmesPath("ultimas_transacoes[0].descricao").ofType[String].is("danada"),
              jmesPath("ultimas_transacoes[0].tipo").ofType[String].is("c"),
              jmesPath("ultimas_transacoes[0].valor").ofType[Int].is("1"),
              jmesPath("saldo.limite").ofType[String].is("#{limite}"),
              jmesPath("saldo.total").ofType[String].is("#{saldo}")
            ),
            http("validações").get("/clientes/#{id}/extrato").check(
              jmesPath("ultimas_transacoes[0].descricao").ofType[String].is("danada"),
              jmesPath("ultimas_transacoes[0].tipo").ofType[String].is("c"),
              jmesPath("ultimas_transacoes[0].valor").ofType[Int].is("1"),
              jmesPath("saldo.limite").ofType[String].is("#{limite}"),
              jmesPath("saldo.total").ofType[String].is("#{saldo}")
            )
        )
    )
  
  .exec(
      http("validações")
      .post("/clientes/#{id}/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s"""{"valor": 1.2, "tipo": "d", "descricao": "devolve"}"""))
          .check(status.in(422, 400))
    )
    .exec(
      http("validações")
      .post("/clientes/#{id}/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s"""{"valor": 1, "tipo": "x", "descricao": "devolve"}"""))
          .check(status.in(422))
    )
    .exec(
      http("validações")
      .post("/clientes/#{id}/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": "123456789 e mais um pouco"}"""))
          .check(status.in(422))
    )
    .exec(
      http("validações")
      .post("/clientes/#{id}/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": ""}"""))
          .check(status.in(422))
    )
    .exec(
      http("validações")
      .post("/clientes/#{id}/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": null}"""))
          .check(status.in(422))
    )

  /* 
    Separar créditos e débitos dá uma visão
    melhor sobre como as duas operações se
    comportam individualmente.
  */
  setUp(
    validacaoTransacoesConcorrentes("d").inject(
      atOnceUsers(validacaConcorrentesNumRequests)
    ).andThen(
      validacaoTransacoesConcorrentesSaldo(validacaConcorrentesNumRequests * -1).inject(
        atOnceUsers(1)
      )
    ).andThen(
      validacaoTransacoesConcorrentes("c").inject(
        atOnceUsers(validacaConcorrentesNumRequests)
      ).andThen(
        validacaoTransacoesConcorrentesSaldo(0).inject(
          atOnceUsers(1)
        )
      )
    ).andThen(
      criteriosClientes.inject(
        atOnceUsers(saldosIniciaisClientes.length)
      ),
      criterioClienteNaoEcontrado.inject(
        atOnceUsers(1)
      ).andThen(
        debitos.inject(
          rampUsersPerSec(1).to(220).during(2.minutes),
          constantUsersPerSec(220).during(2.minutes)
        ),
        creditos.inject(
          rampUsersPerSec(1).to(110).during(2.minutes),
          constantUsersPerSec(110).during(2.minutes)
        ),
        extratos.inject(
          rampUsersPerSec(1).to(10).during(2.minutes),
          constantUsersPerSec(10).during(2.minutes)
        )
      )
    )
  ).protocols(httpProtocol)
}

19) requisitos de negócios

O Que Precisa Ser Feito?

Para participar você precisa desenvolver uma API HTTP com os seguintes endpoints:
Transações

Requisição

POST /clientes/[id]/transacoes

{
    "valor": 1000,
    "tipo" : "c",
    "descricao" : "descricao"
}

Onde

    [id] (na URL) deve ser um número inteiro representando a identificação do cliente.
    valor deve ser um número inteiro positivo que representa centavos (não vamos trabalhar com frações de centavos). 
    -Por exemplo, R$ 10 são 1000 centavos.
    tipo deve ser apenas c para crédito ou d para débito.
    descricao deve ser uma string de 1 a 10 caracteres.

Todos os campos são obrigatórios.

Resposta

HTTP 200 OK

{
    "limite" : 100000,
    "saldo" : -9098
}

Onde

    limite deve ser o limite cadastrado do cliente.
    saldo deve ser o novo saldo após a conclusão da transação.

Obrigatoriamente, o http status code de requisições para transações bem sucedidas deve ser 200!

Regras Uma transação de débito nunca pode deixar o saldo do cliente menor que seu limite disponível. Por exemplo, 
um cliente com limite de 1000 (R$ 10) nunca deverá ter o saldo menor que -1000 (R$ -10). 
Nesse caso, um saldo de -1001 ou menor significa inconsistência na Rinha de Backend!

Se uma requisição para débito for deixar o saldo inconsistente, a API deve retornar HTTP Status Code 422 sem completar 
a transação! O corpo da resposta nesse caso não será testado e você pode escolher como o representar. 
HTTP 422 também deve ser retornado caso os campos do payload estejam fora das especificações como, por exemplo,
 uma string maior do que 10 caracteres para o campo descricao ou algo diferente de c ou d para o campo tipo. Se para o 
 campo valor um número não inteiro for especificado, você poderá retornar HTTP 422 ou 400.

Se o atributo [id] da URL for de uma identificação não existente de cliente, a API deve retornar HTTP Status Code 404.
 O corpo da resposta nesse caso não será testado e você pode escolher como o representar. Se a API retornar algo como 
 HTTP 200 informando que o cliente não foi encontrado no corpo da resposta ou HTTP 204 sem corpo, ficarei extremamente 
 deprimido e a Rinha será cancelada para sempre.
Extrato

Requisição

GET /clientes/[id]/extrato

Onde

    [id] (na URL) deve ser um número inteiro representando a identificação do cliente.

Resposta

HTTP 200 OK

{
  "saldo": {
    "total": -9098,
    "data_extrato": "2024-01-17T02:34:41.217753Z",
    "limite": 100000
  },
  "ultimas_transacoes": [
    {
      "valor": 10,
      "tipo": "c",
      "descricao": "descricao",
      "realizada_em": "2024-01-17T02:34:38.543030Z"
    },
    {
      "valor": 90000,
      "tipo": "d",
      "descricao": "descricao",
      "realizada_em": "2024-01-17T02:34:38.543030Z"
    }
  ]
}

Onde

    saldo
        total deve ser o saldo total atual do cliente (não apenas das últimas transações seguintes exibidas).
        data_extrato deve ser a data/hora da consulta do extrato.
        limite deve ser o limite cadastrado do cliente.
    ultimas_transacoes é uma lista ordenada por data/hora das transações de forma decrescente contendo até as 10 últimas
     transações com o seguinte:
        valor deve ser o valor da transação.
        tipo deve ser c para crédito e d para débito.
        descricao deve ser a descrição informada durante a transação.
        realizada_em deve ser a data/hora da realização da transação.

Regras Se o atributo [id] da URL for de uma identificação não existente de cliente, a API deve retornar HTTP Status Code 404. 
O corpo da resposta nesse caso não será testado e você pode escolher como o representar. Já sabe o que acontece se sua API 
retornar algo na faixa 2XX, né? Agradecido.
Cadastro Inicial de Clientes

Para haver ênfase em concorrência durante o teste, poucos clientes devem ser cadastrados e testados. Por isso, apenas cinco 
clientes, com os seguintes IDs, limites e saldos iniciais, devem ser previamente cadastrados para o teste – isso é imprescindível!
id 	limite 	saldo inicial
1 	100000 	0
2 	80000 	0
3 	1000000 	0
4 	10000000 	0
5 	500000 	0

Obs.: Não cadastre um cliente com o ID 6 especificamente, pois parte do teste é verificar se o cliente com o ID 6 realmente 
não existe e a API retorna HTTP 404!
Como Fazer e Entregar?

Assim como na Rinha de Backend anterior, você precisará conteinerizar sua API e outros componentes usados no formato de 
docker-compose, obedecer às restrições de recursos de CPU e memória, configuração mínima arquitetural, e estrutura de artefatos 
e processo de entrega (o que, onde e quando suas coisas precisam ser entregues).

Você pode fazer a submissão de forma individual, dupla de 2, dupla de 3 ou até dupla de 50 pessoas. Não tem limite. E você e/ou 
seu grupo pode fazer mais de uma submissão desde que a API seja diferente.
Artefato, Processo e Data Limite de Entrega

Para participar, basta fazer um pull request neste repositório incluindo um subdiretório em participantes com os seguintes 
arquivos:

    docker-compose.yml - arquivo interpretável por docker-compose contendo a declaração dos serviços que compõe sua API 
    respeitando as restrições de CPU/memória e arquitetura mínima.
    README.md - incluindo pelo menos seu nome, tecnologias que usou, o link para o repositório do código fonte da sua API, 
    e alguma forma de entrar em contato caso vença. Fique à vontade para incluir informações adicionais como link para site, etc.
    Inclua aqui também quaisquer outros diretórios/arquivos necessários para que seus contêineres subam corretamente como, 
    por exemplo, nginx.conf, banco in-memory, etc.

Aqui tem um exemplo de submissão para te ajudar, caso queira.

Importante! É fundamental que todos os serviços declarados no docker-compose.yml estejam publicamente disponíveis! Caso contrário, 
não será possível executar os testes. Para isso, você pode criar uma conta em hub.docker.com para disponibilizar suas imagens.
 Essa imagens geralmente terão o formato <user>/<imagem>:<tag> – por exemplo, zanfranceschi/rinha-api:latest.

Um erro comum na edição anterior da Rinha foi a declaração de imagens como se estivessem presentes localmente. Isso pode ser 
verdade para quem as construiu (realizou o build localmente), mas não será verdadeiro para o servidor que executará os testes!

Importante! É obrigatório deixar o repositório contendo o código fonte da sua API publicamente acessível e informado no arquivo 
README.md entregue na submissão. Afinal, a Rinha de Backend tem como principal objetivo compartilhar conhecimento!

Submissão/pull request :

         codorna/
        ├── docker-compose.yml
        ├── Dockerfile
        ├── docs
        │   └── API.md
        │   └── arquitetura.sh
        │   └── api.sh
        ├── LICENSE
        ├── load-test
        │   └── user-files
        │       └── executar-teste-local.sh
        │       └── simulations
        │           └── rinhabackend
        │               └── RinhaBackendCrebitosSimulation.scala
        ├── app.sh
        ├── processa.sh
        ├── main.c
        ├── libcodorna.so
        ├── nginx.conf
        └── README.md

A data/hora limite para fazer pull requests para sua submissão é até 2024-03-10T23:59:59-03:00. Após esse dia/hora, qualquer pull request será automaticamente rejeitado.

Note que você poderá fazer quantos pull requests desejar até essa data/hora limite!
Arquitetura Mínima da API

Por "API" aqui, me refiro a todos os serviços envolvidos para que o serviço que atenderá às requisições HTTP funcione, tais como o load balancer, de de objetos in-memory e servidor HTTP.

A sua API precisa ter, no mínimo, os seguintes serviços:

    Um load balancer que faça a distribuição de tráfego usando o algoritmo round robin. Diferentemente da edição anterior, você não precisa usar o Nginx – pode escolher (ou até fazer) qualquer um como p.ex. o HAProxy. O load balancer será o serviço que receberá as requisições do teste e ele precisa aceitar requisições na porta 9999!
    2 instâncias de servidores web que atenderão às requisições HTTP (distribuídas pelo load balancer).
    Um de de objetos in-memory

Restrições de CPU/Memória

Dentro do seu arquivo docker-compose.yml, você deverá limitar todos os serviços para que a soma deles não ultrapasse os seguintes limites:

    deploy.resources.limits.cpu 1.5 – uma unidade e meia de CPU distribuída entre todos os seus serviços
    deploy.resources.limits.memory 550MB – 550 mega bytes de memória distribuídos entre todos os seus serviços

Obs.: Por favor, use MB para unidade de medida de memória; isso facilita as verificações de restrições.

# exemplo de parte de configuração de um serviço dentro do um arquivo docker-compose.yml
...
  nginx:
    image: nginx:latest
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf:ro
    depends_on:
      - api01
      - api02
    ports:
      - "9999:9999"
    deploy:
      resources:
        limits:
          cpus: "0.17"
          memory: "10MB"
...

Arquivos de exemplo para te ajudar

O seguinte são apenas arquivos de exemplo para que você não saia do zero, caso tenha alguma dificuldade ou apenas queira acelerar a construção da sua API. Obviamente, modifique como quiser respeitando todos as restrições anteriormente explicadas aqui. Novamente, você não precisa usar especificamente um de de objetos in-memory – o exemplo seguinte é apenas ilustrativo.

docker-compose.yml

version: "3.5"

services:
  api01: &api
    # Lembre-se de que seu serviço HTTP deve estar hospedado num repositório
    # publicamente acessível! Ex.: hub.docker.com
    image: ze_sobrinho/codorna:latest
    hostname: api01
    environment:
      - DB_HOSTNAME=db # objetos in-memory, desaclopado de db
    
    # Não é necessário expor qualquer porta além da porta do load balancer,
    # mas é comum as pessoas o fazerem para testarem suas APIs e conectarem
    # ao de de objetos in-memory na fase de desenvolvimento.
    ports:
      - "8081:8080"
    depends_on:
      - db
    deploy:
      resources:
        limits:
          cpus: "0.6"
          memory: "200MB"

  api02:
    # Essa sintaxe reusa o que foi declarado em 'api01'.
    <<: *api 
    hostname: api02
    environment:
      - DB_HOSTNAME=db  #db in-memory
    ports:
      - "8082:8080"
 
  nginx:
    image: nginx:latest
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf:ro
    depends_on:
      - api01
      - api02
    ports:
        # Obrigatório expor/usar a porta 9999 no load balancer!
      - "9999:9999" 
    deploy:
      resources:
        limits:
          cpus: "0.17"
          memory: "10MB"

  db:
    image: objetos_in_memory:latest # defina no codigo
    hostname: db
    environment:
      --  # defina para dados in-memory defina consumo de recuros se dentro ou fora de app.sh
    volumes:
      - ./db_inmemory:/docker-entrypoint-initdb.d/script.sh
    deploy:
      resources:
        limits:
          # Note que a soma de todos os limites dos serviços
          # aqui declarados é de 1.5 unidades de CPU e 550MB
          # de memória. A distribuição feita aqui é apenas
          # um exemplo – distribua como quiser.
          cpus: "0.13"
          memory: "140MB"

# O uso do modo `bridge` deve ser adequado à carga que será usada no teste.
# A edição anterior se beneficiou do modo host pois o volume de requisições
# era relativamente alto e a virtualização da rede se tornou um gargalo, mas
# este modo é mais complexo de ser configurado. Fique à vontade para usar o
# modo que quiser desde que não conflite com portas trivialmente usadas em um
# SO.
networks:
  default:
    driver: bridge
    name: rinha-nginx-2024q1

script.sh

-- Coloque scripts iniciais aqui cri objetos na memoria inspirado neste sql
CREATE TABLE...

DO $$
BEGIN
  INSERT INTO clientes (nome, limite)
  VALUES
    ('o barato sai caro', 1000 * 100),
    ('zan corp ltda', 800 * 100),
    ('les cruders', 10000 * 100),
    ('padaria joia de cocaia', 100000 * 100),
    ('kid mais', 5000 * 100);
END; $$

nginx.conf

events {
    worker_connections 1000;
}

http {
    access_log off;
    sendfile   on;
    
    upstream api {
        server api01:8080;
        server api02:8080;
    }

    server {
        listen 9999; # Lembra da porta 9999 obrigatória?
        
        location / {
            proxy_pass http://api;
        }
    }
}

Ferramenta de Teste

Como na edição anterior, a ferramenta Gatling será usada novamente para realizar o teste de performance. 
Pode fazer muita diferença você executar os testes durante a fase de desenvolvimento para detectar possíveis 
problemas e gargalos. O teste está disponível nesse repositório em load-test.
Ambiente de Testes

Para saber os detalhes sobre o ambiente (SO e versões de software) acesse Especificações do Ambiente de Testes.

Note que o ambiente em que os testes serão executados é Linux x64. Portanto, se seu ambiente de desenvolvimento 
possui outra arquitetura, você precisará fazer o build do docker da seguinte forma: $ docker buildx build --platform linux/amd64

Por exemplo: $ docker buildx build --platform linux/amd64 -t zeh_sobrinho/codorna:latest .
Para executar os testes

Aqui estão instruções rápidas para você poder executar os testes:

    ( ) Baixe o Gatling em https://gatling.io/open-source/
    ( ) Certifique-se de que tenha o JDK instalado (64bits OpenJDK LTS (Long Term Support) versions: 11, 17 e 21) 
    https://gatling.io/docs/gatling/tutorials/installation/
    ( ) Certifique-se de configurar a variável de ambiente GATLING_HOME para o diretório da instalação do Gatling. 
    Para se certificar de que a variável está correta, os seguinte caminhos precisam ser válidos: 
    $GATLING_HOME/bin/gatling.sh no Linux e %GATLING_HOME%\bin\gatling.bat no Windows.
    ( ) Configure o script ./executar-teste-local.sh 
    ( ) Suba sua API (ou load balancer) na porta 9999
    ( ) Execute ./executar-teste-local.sh (ou ./executar-teste-local.ps1 se estiver no Windows)
    

De nada :)
Pré teste

Na edição anterior da Rinha, o teste começava poucos segundos após a subida dos contêineres e, devido as restrições 
de CPU e memória, nem todos os serviços estavam prontos para receber requisições em tão pouco tempo. Nessa edição, 
antes do teste iniciar, um script verificará se a API está respondendo corretamente (via GET /clientes/1/extrato) 
por até 40 segundos em intervalos de 2 segundos a cada tentativa. Por isso, certifique-se de que todos seus serviços
 não demorem mais do que 40 segundos para estarem aptos a receberem requisições!
#Nota importante sobre o teste escrito!

A simulação contém um teste de lógica de saldo/limite que extrapola o que é comumente feito em testes de performance. 
O escrevi assim apenas por causa da natureza da Rinha de Backend. Evite fazer esse tipo de coisa em testes de performance, 
pois não é uma prática recomendada normalmente. Testes de lógica devem ficar junto ao código fonte em formato de testes de
 unidade ou integração!
Critérios para Vencer A Rinha de Backend

