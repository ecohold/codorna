#codorna app        
        
        codorna/
        ├── docker-compose.yml
        ├── Dockerfile
        ├── docs
        │   └── API.md
        │   └── arquitetura.sh
        │   └── api.sh
        ├── LICENSE
        ├── load-test
        │   └── user-files
        │       └── executar-teste-local.sh
        │       └── simulations
        │           └── rinhabackend
        │               └── RinhaBackendCrebitosSimulation.scala
        ├── app.sh
        ├── main.c --> libcodorna.so
        ├── libcodorna.so
        ├── nginx.conf
        └── README.md


0) app codorna; api rest crud, in-memory,concorrente, blocante, importa libcodorna.so e se torna mais performaticas que ocê
1) gere código padrão senior develop bash gcp cloud 
2) comentarios somente no código, servir como documentação, auditoria,date, versione, assine 
3) em português  bloco de codigo único , menos de 100 linhas, completo, testado, validado, produção/distribuição/funcional
4) com base na arquitetura e arvore de arquivos do 
5) aplique 23 design pattern e 10xtreme
6) arquivo: nome do arquivo: atenda os requisitos e lógica de negocio e testes. sim
6.1) sim ( )nota de 1 a 3 ( )
6.2) não ( ) porque, onde, linha(s) ( )
6.3) refatore, corrija, supere versão anterior
6.4) teste as mudanças, valide, audite, date, versione, assine, documente em /docs
7) crie o arquivo: nome do arquivo
8) exclua o arquivo: nome do arquivo
9) refatore o arquivo: nome do arquivo 
10)     main.c: api rest crud performatica in-memory completa base para libcodorna.so
10.1)   main.c apoi api rest crud performatica acoplada a db postress persistencia descricao
10.1.1) base para main.c: api rest crud performatica in-memory completa base para libcodorna.so
11) nginx.conf  híbrido (http e proxy reverso) 
12) app.sh: revecebe carga de nginx proxy e roteia para libcodorna.so
13) libcodorna.so: recebe carga repassa processamento para app.sh
14) Dockerfile
15) docker-compose.yml
16) libcodorna.so: cria estrutura de dados, popula, le, altera com base nas requisições de app.sh devolve para 
17) executar-teste-local.sh cliente que gera requisições http na porta 9999 ao nginx load balance
    #runGatlin: 
    1) adicione um docker stats & ps para os processos gatling, java e docker
    2) toda vez que a memória ultrapassar 70%, 80%, 90% e 100%
    3) identificar todos processos e tempo que operaram abaixo de 50%; entre 50 e 60%, entre 60 e 70%,
        entre 70 e 80%, entre 80 e 90%, entre 90 e 100%, entre 100 e 150% entre 150 e 200% (o que acontece?)


#--------------nginx.conf-------------------#

# Proxy Reverso

server {
    listen       9999;  # Porta para ouvir solicitações do proxy reverso (alterada para 9999)
    server_name  aplicacao-externa.com;  # Nome do servidor externo (opcional)

    location / {
        proxy_pass http://127.0.0.1:9999;
        # Repassar solicitações para o serviço externo na porta 8081
        proxy_set_header Host $host;  # Manter o cabeçalho Host original
        proxy_set_header X-Real-IP $remote_addr;  # Incluir o IP real do cliente
    }
}

#------------docker-compose.yml ------------------#

version: "3.5"

services:
 # Serviço Nginx
 nginx:
  image: nginx:latest
  restart: unless-stopped
  ports:
   - 9999:80
  volumes:
   - ./nginx.conf:/etc/nginx/conf.d/default.conf:ro # Monta o arquivo de configuração do Nginx

 # Serviço da aplicação Omelete (ap1)
 ap1:
  build:
   context: ./ # Caminho para o contexto de construção (onde está o Dockerfile)
   dockerfile: Dockerfile
  restart: unless-stopped
  ports:
   - 4000:3000 # Expondo a porta 3000 do container
  environment:
   NGX_SOCKET: ./tmp/nginx_proxy.socket # Socket de comunicação com o Nginx

 # Serviço da aplicação Omelete (ap2)
 ap2:
  build:
   context: ./ # Caminho para o contexto de construção (onde está o Dockerfile)
   dockerfile: Dockerfile
  restart: unless-stopped
  ports:
   - 3001:3000 # Expondo a porta 3001 do container (evitando conflito com ap1)
  environment:
   NGX_SOCKET: ./tmp/nginx_proxy.socket # Socket de comunicação com o Nginx

networks:
 default:
  driver: bridge
  name: omelele

#-----------------app.sh--------------------------#
#!/bin/bash

export LD_PRELOAD=./libcodorna.so

# Função para fazer a requisição HTTP
fazer_requisicao() {
    local url=$1
    local response=$(curl -s "$url")
    echo "$response"
}

# Função para processar a requisição
processar_requisicao() {
    local request=$1
    local method=$(echo "$request" | head -n 1 | cut -d' ' -f1)
    local url=$(echo "$request" | head -n 1 | cut -d' ' -f2)
    local response=""

    case $method in
        GET)
            # Extrai o ID do cliente da URL
            local id=$(echo "$url" | awk -F'/' '{print $3}')
            # Chama função C para GET
            response=$(get_cliente "$id")
            ;;
        POST)
            # Extrai o ID do cliente e o saldo do corpo da requisição
            local id=$(echo "$url" | awk -F'/' '{print $3}')
            local body=$(echo "$request" | tail -n 1)
            # Chama função C para POST 
            response=$(salva_cliente "$id" "$body")
            ;;
    esac

    echo "$response"
}

# Socket do Nginx
ngx_socket=/tmp/nginx_proxy.socket

# Loop infinito recebendo requisições
while socat -u UNIX-LISTEN:$ngx_socket,fork STDOUT; do

    # Lê requisição do Nginx
    request=$(socat - READLINE)
    
    # Faz o processamento da requisição
    response=$(processar_requisicao "$request")

    # Retorna resposta para o Nginx
    echo "$response"

    # Processa requisição localmente
    response=$(processar_requisicao "$request")
    echo "$response"

done

# Pool de processos para escalabilidade
MAX_WORKERS=100

while [[ "$#" -gt 0 ]]; do

    # Processa requisição em background
    processar_requisicao "$1" &

    # Aguarda worker 
    shift
    
    if [[ "${#}" -eq "$MAX_WORKERS" ]]; then
        wait
    fi

done


#----------Dockerfile-------------------------#

# Base image (replace with appropriate image for your language)
FROM ubuntu:20.04

# Install dependencies (adjust based on your requirements)
RUN apt-get update && apt-get install -y make gcc libmicrohttpd-dev libpthread-stubs0-dev socat

# Copy your application code
COPY . .

# Compile the C code (modify the command if needed)
RUN gcc -shared -fPIC -o libcodorna.so main.c -lmicrohttpd -lpthread

# Set working directory
WORKDIR /

# Grant execute permission to app.sh
RUN chmod +x app.sh

# Expose the port used by your application (modify if needed)
EXPOSE 8080

# Command to start the application (replace with your actual command)
CMD ["./app.sh"]

#-----------------main.c----> libcodorna.so------------------#

// gcc -shared -fPIC -o libcodorna.so main.c -lmicrohttpd -lpthread
// main.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <microhttpd.h>
#include <pthread.h> 

#define MAX_CLIENTES 100

typedef struct {
    char id[11];
    int saldo;
} cliente;

cliente clientes[MAX_CLIENTES];
pthread_mutex_t clientes_mutex;

void init_clientes() {
    pthread_mutex_init(&clientes_mutex, NULL);
    for (int i = 0; i < MAX_CLIENTES; i++) {
        strcpy(clientes[i].id, "");
        clientes[i].saldo = 0;
    }
}

int get_cliente(char *id, char *response) {
    pthread_mutex_lock(&clientes_mutex);
    for (int i = 0; i < MAX_CLIENTES; i++) {
        if (strcmp(clientes[i].id, id) == 0) {
            sprintf(response, "{\"id\": \"%s\", \"saldo\": %d}", clientes[i].id, clientes[i].saldo);
            pthread_mutex_unlock(&clientes_mutex);
            return 1; // Cliente encontrado
        }
    }
    pthread_mutex_unlock(&clientes_mutex);
    sprintf(response, "{\"error\": \"Cliente não encontrado\"}");
    return 0; // Cliente não encontrado
}

int salva_cliente(char *id, int saldo) {
    pthread_mutex_lock(&clientes_mutex);
    for (int i = 0; i < MAX_CLIENTES; i++) {
        if (strcmp(clientes[i].id, id) == 0) {
            clientes[i].saldo += saldo;
            pthread_mutex_unlock(&clientes_mutex);
            return 1; // Cliente atualizado
        }
    }
    // Cliente não encontrado, tenta adicionar novo cliente
    for (int i = 0; i < MAX_CLIENTES; i++) {
        if (strcmp(clientes[i].id, "") == 0) {
            strcpy(clientes[i].id, id);
            clientes[i].saldo = saldo;
            pthread_mutex_unlock(&clientes_mutex);
            return 2; // Novo cliente adicionado
        }
    }
    pthread_mutex_unlock(&clientes_mutex);
    return 0; // Falha ao adicionar cliente
}

// Função para lidar com as requisições HTTP
int handle_request(void *cls, struct MHD_Connection *connection, const char *url, const char *method, const char *version,
                   const char *upload_data, size_t *upload_data_size, void **con_cls) {
    char response[1024];
    if (strcmp(method, "GET") == 0) {
        char id[11];
        sscanf(url, "/clientes/%s", id);
        if (get_cliente(id, response)) {
            struct MHD_Response *http_response = MHD_create_response_from_buffer(strlen(response), (void *)response, MHD_RESPMEM_MUST_COPY);
            int ret = MHD_queue_response(connection, MHD_HTTP_OK, http_response);
            MHD_destroy_response(http_response);
            return ret;
        } else {
            struct MHD_Response *http_response = MHD_create_response_from_buffer(strlen(response), (void *)response, MHD_RESPMEM_MUST_COPY);
            int ret = MHD_queue_response(connection, MHD_HTTP_NOT_FOUND, http_response);
            MHD_destroy_response(http_response);
            return ret;
        }
    } else if (strcmp(method, "POST") == 0) {
        char id[11];
        int saldo;
        sscanf(url, "/clientes/%s", id);
        sscanf(upload_data, "saldo=%d", &saldo);
        int result = salva_cliente(id, saldo);
        if (result == 1) {
            sprintf(response, "{\"message\": \"Saldo atualizado para o cliente %s\"}", id);
            struct MHD_Response *http_response = MHD_create_response_from_buffer(strlen(response), (void *)response, MHD_RESPMEM_MUST_COPY);
            int ret = MHD_queue_response(connection, MHD_HTTP_OK, http_response);
            MHD_destroy_response(http_response);
            return ret;
        } else if (result == 2) {
            sprintf(response, "{\"message\": \"Novo cliente adicionado: %s\"}", id);
            struct MHD_Response *http_response = MHD_create_response_from_buffer(strlen(response), (void *)response, MHD_RESPMEM_MUST_COPY);
            int ret = MHD_queue_response(connection, MHD_HTTP_CREATED, http_response);
            MHD_destroy_response(http_response);
            return ret;
        } else {
            sprintf(response, "{\"error\": \"Falha ao atualizar o saldo para o cliente %s\"}", id);
            struct MHD_Response *http_response = MHD_create_response_from_buffer(strlen(response), (void *)response, MHD_RESPMEM_MUST_COPY);
            int ret = MHD_queue_response(connection, MHD_HTTP_INTERNAL_SERVER_ERROR, http_response);
            MHD_destroy_response(http_response);
            return ret;
        }
    } else {
        struct MHD_Response *http_response = MHD_create_response_from_buffer(strlen(response), (void *)response, MHD_RESPMEM_MUST_COPY);
        int ret = MHD_queue_response(connection, MHD_HTTP_METHOD_NOT_ALLOWED, http_response);
        MHD_destroy_response(http_response);
        return ret; 
    }
}

// Função de inicialização do servidor
struct MHD_Daemon *init_server(int port) {
    return MHD_start_daemon(MHD_USE_EPOLL_INTERNAL_THREAD | MHD_USE_EPOLL_TURBO,
                    port, NULL, NULL,
                    (MHD_AccessHandlerCallback) handle_request, // Cast aqui
                    NULL, MHD_OPTION_CONNECTION_TIMEOUT, 30, MHD_OPTION_END);


}