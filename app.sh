#!/bin/bash

export LD_PRELOAD=./libcodorna.so

# Função para fazer a requisição HTTP
fazer_requisicao() {
    local url=$1
    local response=$(curl -s "$url")
    echo "$response"
}

# Função para processar a requisição
processar_requisicao() {
    local request=$1
    local method=$(echo "$request" | head -n 1 | cut -d' ' -f1)
    local url=$(echo "$request" | head -n 1 | cut -d' ' -f2)
    local response=""

    case $method in
        GET)
            # Extrai o ID do cliente da URL
            local id=$(echo "$url" | awk -F'/' '{print $3}')
            # Chama função C para GET
            response=$(get_cliente "$id")
            ;;
        POST)
            # Extrai o ID do cliente e o saldo do corpo da requisição
            local id=$(echo "$url" | awk -F'/' '{print $3}')
            local body=$(echo "$request" | tail -n 1)
            # Chama função C para POST 
            response=$(salva_cliente "$id" "$body")
            ;;
    esac

    echo "$response"
}

# Socket do Nginx
ngx_socket=/tmp/nginx_proxy.socket

# Loop infinito recebendo requisições
while socat -u UNIX-LISTEN:$ngx_socket,fork STDOUT; do

    # Lê requisição do Nginx
    request=$(socat - READLINE)
    
    # Faz o processamento da requisição
    response=$(processar_requisicao "$request")

    # Retorna resposta para o Nginx
    echo "$response"

    # Processa requisição localmente
    response=$(processar_requisicao "$request")
    echo "$response"

done

# Pool de processos para escalabilidade
MAX_WORKERS=100

while [[ "$#" -gt 0 ]]; do

    # Processa requisição em background
    processar_requisicao "$1" &

    # Aguarda worker 
    shift
    
    if [[ "${#}" -eq "$MAX_WORKERS" ]]; then
        wait
    fi

done
