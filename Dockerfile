# Base image (replace with appropriate image for your language)
FROM ubuntu:20.04

# Install dependencies (adjust based on your requirements)
RUN apt-get update && apt-get install -y make gcc libmicrohttpd-dev libpthread-stubs0-dev

# Copy your application code
COPY . .

# Compile the C code (modify the command if needed)
RUN gcc -shared -fPIC -o libcodorna.so main.c -lmicrohttpd -lpthread

# Set working directory
WORKDIR /

# Grant execute permission to app.sh
RUN chmod +x app.sh

# Expose the port used by your application (modify if needed)
EXPOSE 8080

# Command to start the application (replace with your actual command)
CMD ["./app.sh"]  
